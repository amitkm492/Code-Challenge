package com.db.awmd.challenge;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.domain.Transaction;
import com.db.awmd.challenge.service.AccountsService;
import com.db.awmd.challenge.web.Result;

import java.math.BigDecimal;

import javax.validation.constraints.AssertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class AccountsControllerTest {

  private MockMvc mockMvc;

  @Autowired
  private AccountsService accountsService;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @Before
  public void prepareMockMvc() {
    this.mockMvc = webAppContextSetup(this.webApplicationContext).build();

    // Reset the existing accounts before each test.
    accountsService.getAccountsRepository().clearAccounts();
  }

  @Test
  public void createAccount() throws Exception {
    this.mockMvc.perform(post("/v1/accounts").contentType(MediaType.APPLICATION_JSON)
      .content("{\"accountId\":\"Id-123\",\"balance\":1000}")).andExpect(status().isCreated());

    Account account = accountsService.getAccount("Id-123");
    assertThat(account.getAccountId()).isEqualTo("Id-123");
    assertThat(account.getBalance()).isEqualByComparingTo("1000");
  }

  @Test
  public void createDuplicateAccount() throws Exception {
    this.mockMvc.perform(post("/v1/accounts").contentType(MediaType.APPLICATION_JSON)
      .content("{\"accountId\":\"Id-123\",\"balance\":1000}")).andExpect(status().isCreated());

    this.mockMvc.perform(post("/v1/accounts").contentType(MediaType.APPLICATION_JSON)
      .content("{\"accountId\":\"Id-123\",\"balance\":1000}")).andExpect(status().isBadRequest());
  }

  @Test
  public void createAccountNoAccountId() throws Exception {
    this.mockMvc.perform(post("/v1/accounts").contentType(MediaType.APPLICATION_JSON)
      .content("{\"balance\":1000}")).andExpect(status().isBadRequest());
  }

  @Test
  public void createAccountNoBalance() throws Exception {
    this.mockMvc.perform(post("/v1/accounts").contentType(MediaType.APPLICATION_JSON)
      .content("{\"accountId\":\"Id-123\"}")).andExpect(status().isBadRequest());
  }

  @Test
  public void createAccountNoBody() throws Exception {
    this.mockMvc.perform(post("/v1/accounts").contentType(MediaType.APPLICATION_JSON))
      .andExpect(status().isBadRequest());
  }

  @Test
  public void createAccountNegativeBalance() throws Exception {
    this.mockMvc.perform(post("/v1/accounts").contentType(MediaType.APPLICATION_JSON)
      .content("{\"accountId\":\"Id-123\",\"balance\":-1000}")).andExpect(status().isBadRequest());
  }

  @Test
  public void createAccountEmptyAccountId() throws Exception {
    this.mockMvc.perform(post("/v1/accounts").contentType(MediaType.APPLICATION_JSON)
      .content("{\"accountId\":\"\",\"balance\":1000}")).andExpect(status().isBadRequest());
  }

  @Test
  public void getAccount() throws Exception {
    String uniqueAccountId = "Id-" + System.currentTimeMillis();
    Account account = new Account(uniqueAccountId, new BigDecimal("123.45"));
    this.accountsService.createAccount(account);
    this.mockMvc.perform(get("/v1/accounts/" + uniqueAccountId))
      .andExpect(status().isOk())
      .andExpect(
        content().string("{\"accountId\":\"" + uniqueAccountId + "\",\"balance\":123.45}"));
  }
  
  /**
   * Positive scenario
   * 
   * transfer Amount between two Accounts
   * 
   * @throws Exception
   */
  @Test
  public void transferAmount() throws Exception {
	Account account1 = new Account("Id-111");
	account1.setBalance(new BigDecimal(10000));
	this.accountsService.createAccount(account1);
	Account account2 = new Account("Id-222");
	account2.setBalance(new BigDecimal(10000));
	this.accountsService.createAccount(account2);
	this.mockMvc
			.perform(post("/v1/accounts/transfer").contentType(MediaType.APPLICATION_JSON).content(
					"{\"transactionId\" : 1,\"accountFrom\":\"Id-111\",\"accountTo\":\"Id-222\",\"amount\":2000}"))
			.andExpect(status().isCreated());
	Transaction transaction = new Transaction(1L, account1.getAccountId(), account2.getAccountId(),
			new BigDecimal(2000));
	Result result = accountsService.transferAmount(transaction);
	assertFalse(result.hasError());
  }
  
  /**
   * Negative scenario
   * 
   * transfer Amount between two Accounts
   * 
   * @throws Exception
   */
  @Test
  public void transferAmountBadReqException() throws Exception {
	Account account1 = new Account("Id-333");
	account1.setBalance(new BigDecimal(1000));
	this.accountsService.createAccount(account1);
	Account account2 = new Account("Id-444");
	account2.setBalance(new BigDecimal(1000));
	this.accountsService.createAccount(account2);
	this.mockMvc
			.perform(post("/v1/accounts/transfer").contentType(MediaType.APPLICATION_JSON).content(
					"{\"transactionId\" : 1,\"accountFrom\":\"Id-111\",\"accountTo\":\"Id-222\",\"amount\":2000}"))
			.andExpect(status().isBadRequest());
	Transaction transaction = new Transaction(1L, account1.getAccountId(), account2.getAccountId(),
			new BigDecimal(2000));
	Result result = accountsService.transferAmount(transaction);
	assertTrue(result.hasError());
  }

}
