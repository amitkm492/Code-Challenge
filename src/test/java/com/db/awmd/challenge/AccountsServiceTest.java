package com.db.awmd.challenge;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.domain.Transaction;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.service.AccountsService;
import com.db.awmd.challenge.web.Result;

import java.math.BigDecimal;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountsServiceTest {

  @Autowired
  private AccountsService accountsService;

  @Test
  public void addAccount() throws Exception {
    Account account = new Account("Id-123");
    account.setBalance(new BigDecimal(1000));
    this.accountsService.createAccount(account);

    assertThat(this.accountsService.getAccount("Id-123")).isEqualTo(account);
  }

  @Test
  public void addAccount_failsOnDuplicateId() throws Exception {
    String uniqueId = "Id-" + System.currentTimeMillis();
    Account account = new Account(uniqueId);
    this.accountsService.createAccount(account);

    try {
      this.accountsService.createAccount(account);
      fail("Should have failed when adding duplicate account");
    } catch (DuplicateAccountIdException ex) {
      assertThat(ex.getMessage()).isEqualTo("Account id " + uniqueId + " already exists!");
    }

  }
  
  /**
   * Positive scenario
   * 
   * Test Case for Transfer amount between AccountFromID to AccountToID
   *  
   * @throws Exception
   */
  @Test
  public void transferAmount() throws Exception {
    Account account1 = new Account("Acc-111");
    account1.setBalance(new BigDecimal(10000));
    this.accountsService.createAccount(account1);

    Account account2 = new Account("Acc-222");
    account2.setBalance(new BigDecimal(10000));
    this.accountsService.createAccount(account2);

    Transaction transaction = new Transaction(1L,account1.getAccountId(), account2.getAccountId(), new BigDecimal(2000));
    Result result = this.accountsService.transferAmount(transaction);
	assertTrue(!result.hasError());
  }
  
  /**
   * Negative scenario
   * Test Case for Transfer amount between AccountFromID to AccountToID
   * But transfer amount is less than balance
   * 
   * @throws Exception
   */
  @Test
  public void transferAmount_LessBalance() throws Exception {
    Account account1 = new Account("Acc-333");
    account1.setBalance(new BigDecimal(1000));
    this.accountsService.createAccount(account1);

    Account account2 = new Account("Acc-444");
    account2.setBalance(new BigDecimal(1000));
    this.accountsService.createAccount(account2);

    Transaction transaction = new Transaction(1L,account1.getAccountId(), account2.getAccountId(), new BigDecimal(2000));
    Result result = this.accountsService.transferAmount(transaction);
	assertTrue(result.hasError());
  }
  
  /**
   * Negative scenario
   * Test Case for Transfer amount between AccountFromID to AccountToID
   * Null check Exception for AccountFrom Id and AccountTo Id
   * 
   * @throws Exception
   */
  @Test
  public void transferAmount_NullAccFromId_NullAccToId() throws Exception {
    Account account1 = new Account("Acc-555");
    account1.setBalance(new BigDecimal(1000));
    Account account2 = new Account("Acc-666");
    account2.setBalance(new BigDecimal(1000));

    Transaction transaction = new Transaction(1L,account1.getAccountId(), account2.getAccountId(), new BigDecimal(2000));
    Result result = this.accountsService.transferAmount(transaction);
    assertTrue(result.hasError());
  }
}
