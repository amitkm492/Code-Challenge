package com.db.awmd.challenge.repository;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.domain.Transaction;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.web.AccountsController;
import com.db.awmd.challenge.web.Result;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.transaction.AfterTransaction;
@Slf4j
@Repository
public class AccountsRepositoryInMemory implements AccountsRepository {
	 
  private final Map<String, Account> accounts = new ConcurrentHashMap<>();

  @Override
  public void createAccount(Account account) throws DuplicateAccountIdException {
    Account previousAccount = accounts.putIfAbsent(account.getAccountId(), account);
    if (previousAccount != null) {
      throw new DuplicateAccountIdException(
        "Account id " + account.getAccountId() + " already exists!");
    }
  }

  @Override
  public Account getAccount(String accountId) {
    return accounts.get(accountId);
  }

  @Override
  public void clearAccounts() {
    accounts.clear();
  }


/**
 * Function for transfer amount between AccountFromID to AccountToID
 * Validation for transfer amount is less than Balance
 * Validation for Null AccountFrom Id and AccountTo Id
 * 
 * @Transactional used for handle transaction propagation automatically
 */
  
@Override
@Transactional
	public Result transferAmt(Transaction transaction) throws Exception {
		log.info("functionality for a transfer of money between accounts");
		Account accFrom = this.getAccount(transaction.getAccountFrom());
		Account accTo = this.getAccount(transaction.getAccountTo());
		Result res = new Result();
		if ((accFrom != null) && (accTo != null) && (accFrom != accTo)) {
			// Balance amount must be positive and greater than transfer amount
			if ((accFrom.getBalance().subtract(transaction.getAmount()).doubleValue() >= 0)) {
				// Amount is deducted
				accFrom.setBalance(accFrom.getBalance().subtract(transaction.getAmount()));
				// Amount is credited
				accTo.setBalance(accTo.getBalance().add(transaction.getAmount()));
			} else {
				log.error("Amount is less than available Balance");
				res.setError("Amount is less than available Balance", HttpStatus.BAD_REQUEST);

			}
		} else {
			log.error("AccountFromId / AccountToId is invalid! please enter Valid account details.");
			res.setError("AccountFromId / AccountToId is invalid! please enter Valid account details.",
					HttpStatus.BAD_REQUEST);
		}
		return res;
	}
	
}

