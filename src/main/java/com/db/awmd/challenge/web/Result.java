package com.db.awmd.challenge.web;

import org.springframework.http.HttpStatus;

import lombok.Getter;

@Getter
public class Result {
	String message;
	HttpStatus status;
	boolean hasError;
	
	public Result() {
		message = null;
		status = null;
		hasError = false;
	}
	
	public void setError(String message, HttpStatus status) {
		hasError = true;
		this.message = message;
		this.status  = status;
	}
	
	
	public boolean hasError() {
		return hasError;
	}
}
