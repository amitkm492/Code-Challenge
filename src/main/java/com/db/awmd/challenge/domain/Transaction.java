package com.db.awmd.challenge.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

@Data
public class Transaction {

  @NotNull
  private final Long  transactionId;
	
  @NotNull
  @NotEmpty
  private final String accountFrom;

  @NotNull
  @NotEmpty
  private final String accountTo;

  @NotNull
  @Min(value = 1, message = "Amount must be positive and greater than balance.")
  private final BigDecimal amount;

  @JsonCreator
  public Transaction(@JsonProperty("transactionId") Long transactionId,@JsonProperty("accountFrom") String accountFrom,@JsonProperty("accountTo") String accountTo,
    @JsonProperty("amount") BigDecimal amount) {
    this.transactionId = transactionId;
	this.accountFrom = accountFrom;
    this.amount = amount;
    this.accountTo = accountTo;
  }

}
